export class DestinoViaje {
    selected: boolean;
    servicios: string[];

    constructor(public nombre: string, public imagenUrl: string, public votes: number = 0) {
        this.servicios = ['pileta', 'desayuno'];
    }
    setSelected(s: boolean){
        this.selected = s;
    }
    isSelected(){
        return this.selected;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        this.votes--;
    }

}
